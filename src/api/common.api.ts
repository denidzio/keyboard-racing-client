import { IRequest } from '../interfaces';
import { DEVELOPMENT } from '../config';

export const HOST = DEVELOPMENT ? 'http://localhost:3002' : window.origin;

const request = async <T>(request: IRequest): Promise<T> => {
  const { url, method, body } = request;

  const headers = {
    'Content-Type': 'application/json;charset=utf-8',
  };

  const response = await fetch(url, {
    method,
    headers,
    body: JSON.stringify(body),
  });

  if (!response.ok) {
    throw new Error(response.statusText);
  }

  return await response.json();
};

export const get = async <T>(url: string): Promise<T> => {
  return await request({ url, method: 'GET' });
};
