import { ITextResponse } from '../interfaces';

import { get, HOST } from './common.api';

export const getTextById = async (
  id: number,
): Promise<ITextResponse | string> => {
  try {
    return await get<ITextResponse>(`${HOST}/game/texts/${id}`);
  } catch (error) {
    return error;
  }
};
