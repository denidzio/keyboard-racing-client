import { IPlayer, IRoom } from '../interfaces';
import { getSessionUsername } from './username.helper';

export const sortPlayers = (players: IPlayer[]) => {
  return players.sort((a, b) => {
    const usernameA = a.user.username;
    const usernameB = b.user.username;

    if (usernameA < usernameB) {
      return -1;
    }

    if (usernameA > usernameB) {
      return 1;
    }

    return 0;
  });
};

export const getCurrentPlayerInRoom = (room: IRoom) => {
  const username = getSessionUsername();

  if (!username) {
    return;
  }

  return room.players.find((player) => player.user.username === username);
};
