export const setSessionStorageItem = (key: string, value: any): void => {
  let validValue: string;

  if (typeof value === 'object') {
    validValue = JSON.stringify(value);
  } else {
    validValue = String(value);
  }

  sessionStorage.setItem(key, validValue);
};

export const getObjectFromSessionStorage = (key: string): any => {
  const data = sessionStorage.getItem(key);

  if (!data) {
    return null;
  }

  return JSON.parse(data);
};
