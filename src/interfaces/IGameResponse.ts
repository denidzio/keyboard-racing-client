export default interface IGame {
  timerBeforeGame: number;
  timerDuringGame: number;
  textId: number;
}
