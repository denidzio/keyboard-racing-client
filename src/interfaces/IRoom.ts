import { IPlayer } from './';

export default interface IRoom {
  name: string;
  players: IPlayer[];
}
