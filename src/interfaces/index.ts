export type { default as IUser } from './IUser';
export type { default as IPlayer } from './IPlayer';
export type { default as IRoom } from './IRoom';
export type { default as IGame } from './IGame';
export type { default as IGameResponse } from './IGameResponse';
export type { default as IDictionary } from './IDictionary';
export type { default as IRequest } from './IRequest';
export type { default as ITextResponse } from './ITextResponse';
