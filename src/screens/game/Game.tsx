import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router';
import { Socket } from 'socket.io-client';
import { DefaultEventsMap } from 'socket.io-client/build/typed-events';

import { IRoom } from '../../interfaces';
import { Rooms, Room } from './components';
import { openConnection } from '../../socket/socket';
import {
  getSessionUsername,
  setSessionUsername,
} from '../../helpers/username.helper';

function Game() {
  const [isRedirect, setIsRedirect] = useState(false);
  const [pageIsLoaded, setPageIsLoaded] = useState(false);

  const [currentRoom, setCurrentRoom] = useState<IRoom>();
  const [socket, setSocket] =
    useState<Socket<DefaultEventsMap, DefaultEventsMap>>();

  const redirect = () => {
    setIsRedirect(true);
  };

  const handleDisconnect = (reason: string) => {
    setCurrentRoom(undefined);
  };

  const handleUsernameIsBusy = () => {
    alert('Chosen username is busy!');

    setSessionUsername(null);
    redirect();
  };

  const handleRoomExit = () => {
    setCurrentRoom(undefined);
  };

  useEffect(() => {
    const username = getSessionUsername();

    if (!username) {
      redirect();
      return;
    }

    const io = openConnection(username);

    if (io) {
      setSocket(io);
    }

    setPageIsLoaded(true);
  }, []);

  useEffect(() => {
    if (!socket) {
      return;
    }

    socket.on('USERNAME_IS_BUSY', handleUsernameIsBusy);
    socket.on('UPDATE_ROOM', setCurrentRoom);
    socket.on('disconnect', handleDisconnect);

    return () => {
      socket.offAny();
    };
  }, [socket]);

  if (isRedirect) {
    return <Redirect to="/login" />;
  }

  if (!socket || !pageIsLoaded) {
    return null;
  }

  return currentRoom ? (
    <Room room={currentRoom} socket={socket} onExit={handleRoomExit} />
  ) : (
    <Rooms socket={socket} onJoinToRoom={setCurrentRoom} />
  );
}

export default Game;
