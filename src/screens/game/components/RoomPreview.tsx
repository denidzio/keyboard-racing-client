import React from 'react';
import { IRoom } from '../../../interfaces';

function RoomPreview({ data, onJoin }: { data: IRoom; onJoin: () => void }) {
  return (
    <section className="room room-preview">
      <fieldset className="_room-preview-container">
        <legend className="room-preview__legend room-preview-legend">
          {data.players.length} users connected
        </legend>
        <h2 className="room-preview__body room-preview-body">{data.name}</h2>
        <footer className="room-preview__footer room-preview-footer">
          <button className="room-preview-button join-btn" onClick={onJoin}>
            Join
          </button>
        </footer>
      </fieldset>
    </section>
  );
}

export default RoomPreview;
