import React, { useState, useEffect } from 'react';
import { Socket } from 'socket.io-client';
import { DefaultEventsMap } from 'socket.io-client/build/typed-events';

import { IRoom } from '../../../interfaces';
import { RoomPreview } from './';

function Rooms({
  socket,
  onJoinToRoom = () => {},
}: {
  socket: Socket<DefaultEventsMap, DefaultEventsMap>;
  onJoinToRoom: (room: IRoom) => void;
}) {
  const [rooms, setRooms] = useState<IRoom[]>([]);

  useEffect(() => {
    if (!socket) {
      return;
    }

    socket.on('UPDATE_ROOMS', setRooms);
    socket.on('ROOM_NAME_IS_BUSY', () => alert('Room name is busy!'));
    socket.on('JOIN_TO_ROOM', onJoinToRoom);

    return () => {
      socket.offAny();
    };
  }, [socket]);

  const handleCreateRoom = () => {
    const roomName = prompt('Please input room name:', '');

    if (!roomName || !roomName.trim()) {
      return;
    }

    socket.emit('ADD_ROOM', roomName.trim());
  };

  const handleJoinToRoom = (name: string) => {
    socket.emit('JOIN_TO_ROOM', name);
  };

  return (
    <div className="rooms _container" id="rooms-page">
      <header className="rooms-header">
        <h1 className="rooms-header__title">Join Room or Create New</h1>
        <button onClick={handleCreateRoom} id="add-room-btn">
          Create Room
        </button>
      </header>
      <div className="rooms__body rooms-body">
        {rooms.map((room) => (
          <RoomPreview
            data={room}
            key={room.name}
            onJoin={() => handleJoinToRoom(room.name)}
          />
        ))}
      </div>
    </div>
  );
}

export default Rooms;
