import React from 'react';
import { getSessionUsername } from '../../../helpers/username.helper';
import { IPlayer } from '../../../interfaces';

function UserProgress({
  className = '',
  player,
}: {
  className?: string;
  player: IPlayer;
}) {
  const getTextUsername = () => {
    const username = player.user.username;
    return username + (getSessionUsername() === username ? ' (you)' : '');
  };

  const getReadyClassName = () => {
    return player.isReady ? 'ready-status-green' : 'ready-status-red';
  };

  const getProgressClassName = () => {
    return `user-progress ${player.user.username} ${
      player.progress === 100 ? 'finished' : ''
    }`;
  };

  return (
    <fieldset className={`user-progress-wrapper ${className}`}>
      <legend
        className={`user-in-game user-progress-wrapper__user-in-game ${getReadyClassName()}`}
      >
        {getTextUsername()}
      </legend>
      <div className="progress-bar">
        <div
          className={getProgressClassName()}
          style={{ width: `${player.progress}%` }}
        ></div>
      </div>
    </fieldset>
  );
}

export default UserProgress;
