import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router';

import { LoginForm } from './components';
import {
  setSessionUsername,
  getSessionUsername,
} from '../../helpers/username.helper';

function Login() {
  const [isRedirect, setIsRedirect] = useState(false);
  const [pageIsLoaded, setPageIsLoaded] = useState(false);

  const redirect = () => {
    setIsRedirect(true);
  };

  useEffect(() => {
    if (getSessionUsername()) {
      redirect();
      return;
    }

    setPageIsLoaded(true);
  }, []);

  const handleLogin = (username: string) => {
    if (username.trim() === '') {
      return;
    }

    setSessionUsername(username.trim());
    redirect();
  };

  if (isRedirect) {
    return <Redirect to="/game" />;
  }

  return pageIsLoaded ? (
    <div className="login">
      <LoginForm className="login__form" onSubmit={handleLogin} />
    </div>
  ) : null;
}

export default Login;
