import React, { useState, SyntheticEvent, FormEvent } from 'react';

function LoginForm({
  className = '',
  onSubmit = () => {},
}: {
  className: string;
  onSubmit: (username: string) => void;
}) {
  const [username, setUsername] = useState('');

  const handleInput = (e: FormEvent<HTMLInputElement>) => {
    setUsername(e.currentTarget.value);
  };

  const handleSubmit = (e: SyntheticEvent) => {
    e.preventDefault();
    onSubmit(username);
  };

  return (
    <form onSubmit={handleSubmit} className={`login-form ${className}`}>
      <label htmlFor="username-input" className="login-form__label">
        <input
          id="username-input"
          placeholder="Username"
          value={username}
          onChange={handleInput}
        />
      </label>
      <button type="submit" id="submit-button">
        Submit
      </button>
    </form>
  );
}

export default LoginForm;
