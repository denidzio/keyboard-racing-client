import { ITextResponse } from '../interfaces';

import { getTextById as getTextByIdApi } from '../api/text.api';

export const getTextById = async (id: number): Promise<ITextResponse> => {
  const text = await getTextByIdApi(id);

  if (typeof text === 'string') {
    return { text };
  }

  return text;
};
